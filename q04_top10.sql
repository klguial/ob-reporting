-- **********************************
-- TOP 10 INDICATIONS

WITH abdominal_Cases as (


SELECT
    'Primary LSCS' as Type
    ,CASE
        WHEN RESNCS=0 THEN 'NOT INDICATED'
        ELSE RESNCS
    END as Indications
    ,COUNT(DISTINCT MOTHERID) as Count
    ,ROUND(
        COUNT(DISTINCT MOTHERID) / CAST( (SELECT COUNT(DISTINCT MOTHERID) FROM CASES WHERE ROUTEDEL=2) AS FLOAT)
    ,3) as Percent
FROM CASES
WHERE 1=1
AND ROUTEDEL=2
AND mandel IN ('B1','A1')
GROUP BY 1,2

UNION
SELECT
    'Repeat LSCS' as Type
    ,CASE
        WHEN RESNCS=0 THEN 'NOT INDICATED'
        ELSE RESNCS
    END as Indications
    ,COUNT(DISTINCT MOTHERID) as Count
    ,ROUND(
        COUNT(DISTINCT MOTHERID) / CAST( (SELECT COUNT(DISTINCT MOTHERID) FROM CASES WHERE ROUTEDEL=2) AS FLOAT)
    ,3) as Percent
FROM CASES
WHERE 1=1
AND ROUTEDEL=2
AND mandel IN ('A3','B3')
GROUP BY 1,2

UNION
SELECT
    'Primary Classical CS' as Type
    ,CASE
        WHEN RESNCS=0 THEN 'NOT INDICATED'
        ELSE RESNCS
    END as Indications
    ,COUNT(DISTINCT MOTHERID) as Count
    ,ROUND(
        COUNT(DISTINCT MOTHERID) / CAST( (SELECT COUNT(DISTINCT MOTHERID) FROM CASES WHERE ROUTEDEL=2) AS FLOAT)
    ,3) as Percent
FROM CASES
WHERE 1=1
AND ROUTEDEL=2
AND mandel IN ('B2')
GROUP BY 1,2

UNION
SELECT
    'Repeat Classical CS' as Type
    ,CASE
        WHEN RESNCS=0 THEN 'NOT INDICATED'
        ELSE RESNCS
    END as Indications
    ,COUNT(DISTINCT MOTHERID) as Count
    ,ROUND(
        COUNT(DISTINCT MOTHERID) / CAST( (SELECT COUNT(DISTINCT MOTHERID) FROM CASES WHERE ROUTEDEL=2) AS FLOAT)
    ,3) as Percent
FROM CASES
WHERE 1=1
AND ROUTEDEL=2
AND mandel IN ('A2')
GROUP BY 1,2


UNION
SELECT
    'CS Hysterectomy' as Type
    ,CASE
        WHEN RESNCS=0 THEN 'NOT INDICATED'
        ELSE RESNCS
    END as Indications
    ,COUNT(DISTINCT MOTHERID) as Count
    ,ROUND(
        COUNT(DISTINCT MOTHERID) / CAST( (SELECT COUNT(DISTINCT MOTHERID) FROM CASES WHERE ROUTEDEL=2) AS FLOAT)
    ,3) as Percent
FROM CASES
WHERE 1=1
AND ROUTEDEL=2
AND mandel IN ('E1')
GROUP BY 1,2

UNION
SELECT
    'Others' as Type
    ,CASE
        WHEN RESNCS=0 THEN 'NOT INDICATED'
        ELSE RESNCS
    END as Indications
    ,COUNT(DISTINCT MOTHERID) as Count
    ,ROUND(
        COUNT(DISTINCT MOTHERID) / CAST( (SELECT COUNT(DISTINCT MOTHERID) FROM CASES WHERE ROUTEDEL=2) AS FLOAT)
    ,3) as Percent
FROM CASES
WHERE 1=1
AND ROUTEDEL=2
AND mandel NOT IN ('A1','A3','A2','B1','B2','B2','E1')
GROUP BY 1,2


)



SELECT *
FROM
(
    SELECT  Type,Indications,Count,Percent
        ,Row_Number() Over (partition by Type order by Count desc) as rank
    FROM abdominal_Cases
    WHERE Indications<>'NOT INDICATED'
)

UNION
SELECT Type,Indications,Count,Percent,99 as rank
FROM abdominal_Cases
WHERE Indications='NOT INDICATED'

ORDER BY Type, rank
-- WHERE RN<=10