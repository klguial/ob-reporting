-- **********************************
-- Fetal/Maternal Causes Data
-- 1: FETAL
-- 2: MATERNAL

SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Total' as L1_CAUSE
        ,null as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT

FROM Cases
GROUP BY 1,2,3,4,5,6

UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Congenital' as L1_CAUSE
        ,null as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT

FROM Cases
WHERE (FET1=4 OR FET1=4 OR FET3=4) OR FCONG=2 OR (OB1=5 OR OB2=5 OR OB3=5)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Antepartum complications' as L1_CAUSE
        ,null as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE DISBB=2 AND CHIEFCOMPLAINT=18
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Intrapartum complications' as L1_CAUSE
        ,null as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE DISBB=2 AND CHIEFCOMPLAINT<>18
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Complications of prematurity' as L1_CAUSE
        ,null as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND AOGDEL<28
AND (
    GES = 1
    OR (FET1 in (8,9) OR FET2 in (8,9) OR FET3 IN (8,9))
    OR (OB1=11 OR OB2=11 OR OB3=11)
    OR (MEM1=4 OR MEM2=4) = 4
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Infections' as L1_CAUSE
        ,null as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (
    MEM1=4 OR MEM2=4
    OR (AMF1=3 OR AMF2=3)
    OR (INFN1>0 OR INFN2>0)
    OR (RES1=5 OR RES2=5)
    OR (GIT1 IN (1,4,5,6) OR GIT2 IN (1,4,5,6))
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'Malpresentation' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (MPR1>0 OR MPR2>0)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'Fetal injuries & anomalies' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (
    FET1 IN (3,10,11,12)
    OR FET2 IN (3,10,11,12)
    OR FET3 IN (3,10,11,12)
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'Hydramnios' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (
    AMF1 IN (1,2,4) OR AMF2 IN (1,2,4)
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'Pulmonary' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND ( (RES1>0 AND RES1<>5) OR (RES2>0 AND RES2<>5) )
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'CVS' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (CVS1>0 OR CVS2>0)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'GI' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (
    GIT1 IN (2,3,7) OR GIT2 IN (2,3,7)
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'CNS' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (CNS1>0 OR CNS2>0)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'Hema' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (HEM1>0 OR HEM2>0)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'Hema' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (MET1>0 OR MET2>0)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,1 as CASETYPE
        ,'Other causes' as L1_CAUSE
        ,'Hema' as L2_CAUSE
        ,COUNT(DISTINCT FETALID) as COUNT
FROM Cases
WHERE 1=1
AND (TRAUMA1>0 OR TRAUMA2>0)
GROUP BY 1,2,3,4,5,6


-----------------
-- MATERNAL CAUSES
-----------------
UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Total' as L1_CAUSE
        ,null as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
GROUP BY 1,2,3,4,5,6

UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Direct maternal causes' as L1_CAUSE
        ,'Hemorrhage' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    ( HRG1>0 OR HRG2>0 )
    OR ( DURDEL1 IN (7,12,13) OR DURDEL2 IN (7,12,13) )
    OR (PPHRG1>0 AND PPHRG1 NOT IN (5,9) )
    OR (PPHRG2>0 AND PPHRG2 NOT IN (5,9) )
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Direct maternal causes' as L1_CAUSE
        ,'Hypertension' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (HPN1>0 OR HPN2>0)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Direct maternal causes' as L1_CAUSE
        ,'Infection' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    ( (INF1>0 AND INF1 NOT IN (14,15,16,17,18,19)) OR (INF2>0 AND INF2 NOT IN (14,15,16,17,18,19)) )
    OR MUS1=2 OR MUS2=2
    OR PPHRG1=9 OR PPHRG2=9
    OR ( PUER1 IN (9,11) OR PUER2 IN (9,11) )
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Direct maternal causes' as L1_CAUSE
        ,'COVID' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    INF1 IN (14,15,16,17,18,19) OR INF2 IN (14,15,16,17,18,19)
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Direct maternal causes' as L1_CAUSE
        ,'Vascular accident' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    PUER1 IN (1,2,5,6,7) OR PUER2 IN (1,2,5,6,7)
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Direct maternal causes' as L1_CAUSE
        ,'Anesthesia' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (PPHRG1=5 OR PPHRG2=5)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Cardiac' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (CAR1>0 OR CAR2>0 OR CAR3>0)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Vascular' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    ( VEN1>0 OR VEN2>0 )
    OR ( PUER1 IN (3,8,12) OR PUER2 IN (3,8,12) )
    OR ( RHE1=3 OR RHE2=3 )
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Reproductive tract' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    ( GYN1>0 OR GYN2>0 )
    OR ( (DURDEL1 IN (1,2,3,4,5,6,11,14)) OR DURDEL2 IN (1,2,3,4,5,6,11,14) )
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Urinary tract' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    ( REN1>0 OR REN2>0 )
    OR ( SUR1=3 OR SUR1=3 )
    OR ( DURDEL1 IN (9,10) OR DURDEL2 IN (9,10) )
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Hepatic' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    (LIV1>0 AND LIV1<>7)
    OR (LIV2>0 AND LIV2<>7)
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Pulmonary' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    PUL1>0 OR PUL2>0 OR PUL3>0
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Metabolic' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    END1>0 OR END2>0 OR END3>0
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Hematologic' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    HEM1>0 OR HEM2>0
)
GROUP BY 1,2,3,4,5,6

UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Gastrointestinal' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    ( SUR1 IN (1,2) OR SUR2 IN (1,2) )
    OR (LIV1=7 OR LIV2=7)
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Indirect maternal causes' as L1_CAUSE
        ,'Malignancy' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (
    ( MAL1>0 OR MAL2>0 )
    OR ( GYN1 IN (3,4) OR GYN2 IN (3,4) )
)
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Non-maternal causes' as L1_CAUSE
        ,'Accidents' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (SUR1 IN (5,6) OR SUR2 IN (5,6))
GROUP BY 1,2,3,4,5,6


UNION
SELECT  DATEDEL
        ,CASE
            WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
            WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
            ELSE DISBB
        END as DISBB
        ,DISMOTH
        ,2 as CASETYPE
        ,'Non-maternal causes' as L1_CAUSE
        ,'Murder' as L2_CAUSE
        ,COUNT(DISTINCT MOTHERID) as COUNT
FROM Cases
WHERE 1=1
AND (SUR1=4 OR SUR2=4)
GROUP BY 1,2,3,4,5,6

