-- **********************************
-- Deliveries Data
SELECT DATE,DATEADM,DATEDEL,CASENO,MOTHERID,FETALID,PREGDEL,BBSEX,DISMOTH,DISBB
    ,MATDIS,FETDIS,CONGAN,NUMFET
    ,PREN
    ,NOPREN
    ,MEDHIST
    ,DYS,HRG,MEM,AMF,DURDEL,PUER,PPHRG
    ,AGE,BBWT,AOGDEL
    ,CASE
        WHEN BMI IS NOT NULL THEN 
            (CASE
                WHEN BMI < 18.5 THEN 'UW'
                WHEN BMI BETWEEN 18.5 AND 24.9 THEN 'N'
                WHEN BMI BETWEEN 25 AND 29.9 THEN 'OW'
                WHEN BMI BETWEEN 30 AND 40 THEN 'OB1'
                WHEN BMI > 40 THEN 'OB2'
            END)
        ELSE NULL
    END AS BMI
    ,PA
    ,ROUTEDEL
    ,MANDEL
    ,VBAC
    ,EPISIOTOMY
    ,PERINEAL
    ,BTL
    ,ANES
    ,MPR1
    ,MPR2
    ,PREVCS
    ,DEXAMG
    ,END1
    ,HPN1
    ,HPN2
    ,POH1
    ,POH2
    ,POH3
    ,GES
FROM (

SELECT  DATE,DATEADM,DATEDEL,CASENO,MOTHERID,FETALID,PREGDEL,BBSEX,DISMOTH
    ,CASE
        WHEN DISBB=2 AND CHIEFCOMPLAINT=18 THEN 5
        WHEN DISBB=2 AND CHIEFCOMPLAINT<>18 THEN 6
        ELSE DISBB
    END AS DISBB
    ,MATDIS
    ,CASE
        WHEN (FET1 NOT IN (0,2,5,6,7)
                OR FET2 NOT IN (0,2,5,6,7)
                OR FET3 NOT IN (0,2,5,6,7)
            ) THEN 1
        ELSE FETDIS
    END as FETDIS
    ,CASE
        WHEN (FET1=4 OR FET1=4 OR FET3=4) THEN 1
        ELSE 0
    END as CONGAN
    ,CASE
        WHEN PREGDEL=1 THEN
            (CASE
                WHEN (FET1 IN (6,7) OR FET2 IN (6,7) OR FET3 IN (6,7)) THEN 3
                WHEN (FET1 IN (5,11,51)
                        OR FET2 IN (5,11,51)
                        OR FET3 IN (5,11,51)
                    ) THEN 2
                WHEN (FET1 IN (0,1,2,3,4,8,9,10,12)
                        AND FET2 IN (0,1,2,3,4,8,9,10,12)
                        AND FET3 IN (0,1,2,3,4,8,9,10,12)
                    ) THEN 1
            END)
        ELSE 0
    END as NUMFET
    ,CASE WHEN (DYS1+DYS2)>0 THEN 1 ELSE 0 END as DYS
    ,CASE
        WHEN (HRG1+HRG2)>0 THEN 1
        WHEN (DURDEL1 IN (7,12,13) OR DURDEL2 IN (7,12,13)) THEN 1
        ELSE 0
    END as HRG
    ,CASE WHEN (MEM1+MEM2)>0 THEN 1 ELSE 0 END as MEM
    ,CASE WHEN (AMF1+AMF2)>0 THEN 1 ELSE 0 END as AMF
    ,CASE WHEN (
            DURDEL1 NOT IN (7,12,13)
            AND DURDEL2 NOT IN (7,12,13)
            AND (DURDEL1+DURDEL2)>0
    ) THEN 1 ELSE 0 END as DURDEL
    ,CASE WHEN (PUER1+PUER2)>0 THEN 1 ELSE 0 END as PUER
    ,CASE WHEN (PPHRG1+PPHRG2)>0 THEN 1 ELSE 0 END as PPHRG
    ,AGE,COALESCE(BBWT,0) BBWT,AOGDEL
    ,WTKG
    ,HTCM
    ,ROUND(WTKG / POWER((HTCM/100.),2),1) AS BMI
    ,PA
    ,ROUTEDEL
    ,MANDEL
    ,VBAC
    ,EPISIOTOMY
    ,PERINEAL
    ,BTL
    ,ANES
    ,MPR1
    ,MPR2
    ,CASE
        WHEN MEDHIST NOT IN (1,2,3,4,6) THEN 7
        ELSE MEDHIST
    end as MEDHIST
    ,PREN
    ,NOPREN
    ,CASE
        WHEN (OBS1=2 OR OBS2=2 OR OBS3=2 OR OBS4=2) THEN 1
        ELSE 0
    END as PREVCS
    ,DEXAMG
    ,END1
    ,HPN1
    ,HPN2
    ,POH1
    ,POH2
    ,POH3
    ,GES
FROM Cases

)