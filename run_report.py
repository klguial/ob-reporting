# Authors: Guial, Bacong

import os
import sqlite3
import configparser
import csv
import logging
import json
import datetime
import hashlib
import pandas as pd
from openpyxl import load_workbook
import re

# Each row kasi ha table is bagat admission ha clinic, regardless kun nanganak (pregdel = 1) or waray (pregdel = 0).
# Each fetus hit mother (caseno, lastname, firstname) is naka entry as row. So if >2 fetus, >2 gihap it entry ni mother
# an disbb not necessarily =1 kay pwede 2 (stillbirth) or patay (3&4) hiya.

MODULE_PATH = os.getcwd()
MODULE_NAME = os.path.basename(__file__)

config = configparser.ConfigParser()
config.read('config.ini')
DATA_FILENAME = config['FilePaths']['DATA_FILENAME']
LEGEND_FILENAME = config['FilePaths']['LEGEND_FILENAME']
MORTALITY_CASE_FILENAME = config['FilePaths']['MORTALITY_CASE_FILENAME']
DB_NAME = config['FilePaths']['DB_NAME']
LOG_FILENAME = 'output.log'
DEFAULTS_FILENAME = 'defaults.json'

logging.basicConfig(filename=os.path.join(MODULE_PATH,LOG_FILENAME), level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

CASES_DEFAULT_VALUES = {}
try:
    with open('defaults.json', 'r') as file:
        CASES_DEFAULT_VALUES = json.loads(file.read())
except:
    logging.WARNING('Did not create CASES_DEFAULT_VALUES object...')

db_path = os.path.join(MODULE_PATH,DB_NAME)
if os.path.exists(db_path):
    os.remove(db_path)
conn = sqlite3.connect(db_path)
cursor = conn.cursor()

regex_pattern = r'[^a-zA-Z0-9\s]'

fetal_conditions = ['GES','RES1','RES2','CVS1','CVS2','CNS1','CNS2','HEME1','HEME2','MET1','MET2','INFN1','INFN2','TRAUMA1','TRAUMA2']
maternal_conditions = ['HPN1','HPN2','VEN1','VEN2','REN1','REN2','INF1','INF2','END1','END2','END3','LIV1','LIV2','NEU1','NEU2','HEM1','HEM2','CAR1','CAR2','CAR3','PUL1','PUL2','PUL3','SUR1','SUR2','GYN1','GYN2','MAL1','MAL2','RHE1','RHE2','MUS1','MUS2','POH1','POH2','POH3','POH4']
obstetrics = ['OBS1','OBS2','OBS3','OBS4']

def generate_unique_id(string):
    hash_object = hashlib.md5()
    hash_object.update(string.encode('utf-8'))
    unique_id = hash_object.hexdigest()
    return unique_id


def setupLocalDB():

    fpath = os.path.join(MODULE_PATH,'data',DATA_FILENAME)
    
    addtl_cols = ['DATE','MATDIS','FETDIS','MOTHERID','FETALID']
    
    wb = load_workbook(fpath)
    sheet = wb.worksheets[0]
    colNames = [cell.value.replace(' ', '').upper() for cell in next(sheet.iter_rows())]
    colNames.extend(addtl_cols)

    logging.info("CREATING CASES Table..")
    sql = """ CREATE TABLE IF NOT EXISTS cases (%s) """ % ','.join(colNames)
    cursor.execute(sql)
    conn.commit()
    
    logging.info("CREATING LEGEND Table..")
    sql = """ CREATE TABLE IF NOT EXISTS legend (header TEXT, key TEXT, value TEXT) """
    cursor.execute(sql)
    conn.commit()

    logging.info("CREATING MATERNITYMORTALITYCAUSETABLE Table..")
    sql = """ CREATE TABLE IF NOT EXISTS maternityMortalityCause (type TEXT, antecedent TEXT, underlying TEXT) """
    cursor.execute(sql)
    conn.commit()

    return colNames


def set_defaults(record):
    for key,val in CASES_DEFAULT_VALUES.items():
        try:
            if record[key] is None:
                record[key] = val
        except:
            continue
    return record


def insertCasesData(colNames):
    fpath = os.path.join(MODULE_PATH,'data',DATA_FILENAME)
    wb = load_workbook(fpath)
    numCols = len(colNames)
    rownum =  0

    for sheet in wb.worksheets:
        cases_data = []
        _iter = sheet.iter_rows()
        next(_iter)

        for row in _iter:
            vals = list(map(lambda cell: cell.value, row[:numCols]))
            record = dict(zip(colNames, vals))

            if record['CASENO']:
                rownum += 1
                
                # Date
                date = datetime.date(int(record['DATEADM2']), int(record['DATEADM']), int(record['DATEADM1']))
                if int(record['DATEADM2'])==2024:
                    print(date, record['CASENO'])
                record['DATE'] = date.strftime("%Y-%m-%d")
                
                caseno = str(record['CASENO']).replace(' ','')
                lastname = re.sub(regex_pattern, '', record['LASTNAME']).replace(' ','')
                firstname = re.sub(regex_pattern, '', record['FIRSTNAME']).replace(' ','')

                # MotherID
                record['MOTHERID'] = "mom-%s" % (
                                            generate_unique_id("{}{}{}".format(caseno,lastname,firstname))
                                        )
                
                # FetalID
                record['FETALID'] = None
                if record['PREGDEL']==1 and record['DISBB']:
                    record['FETALID'] = "ftl-%s" % (
                                                generate_unique_id("{}{}{}-{}".format(caseno,lastname,firstname,rownum))
                                            )
                any_fetal = any([record[cond]!=1 if record[cond]=='FCONG' else record[cond]!=0 for cond in fetal_conditions])
                any_maternal = any([record[cond]!=0 for cond in maternal_conditions])
                any_fetal_obstetrics = any([record[cond] in (5,9,10,11,12) for cond in obstetrics])
                any_maternal_obstetrics = any([record[cond] in (1,3) for cond in obstetrics])

                # FETDIS
                if ( record['PREGDEL']== 1 and  (any_fetal or any_fetal_obstetrics) ):
                    record['FETDIS'] = 1
                else:
                    record['FETDIS'] = 0

                # MATDIS
                if ( record['PREGDEL']==1 and (any_maternal or any_maternal_obstetrics) ):
                    record['MATDIS'] = 1
                else:
                    record['MATDIS'] = 0

                # MANDEL CLEANING
                if isinstance(record['MANDEL'], str):
                    tmp = record['MANDEL'].split(',')[0]
                    tmp = 'B1' if tmp=='BI' else tmp
                    record['MANDEL'] = tmp

                # 
                record = set_defaults(record)
                vals = [record.get(column) for column in colNames]
                cases_data.append(vals)

        # cases_df = pd.DataFrame(cases_data, columns=colNames)
        # print(cases_df.head())
        logging.info('INSERTING data to CASES...')
        cursor.executemany('INSERT INTO cases ({}) VALUES ({})'.format(', '.join(colNames), ', '.join('?' * len(colNames))), cases_data)
    conn.commit()


def insertLegendData():
    fpath = os.path.join(MODULE_PATH,'data',LEGEND_FILENAME)
    wb = load_workbook(fpath)
    sheet = wb.worksheets[0]
    data = []
    for col in sheet.iter_cols():
        header = col[0].value.replace(' ', '')
        for cell in col[1:]:
            text = cell.value
            try:
                if text and text.upper().find('VARIES'):
                    key,value = text.split('-', 1)
                    data.append((header, key, value))
            except AttributeError:
                logging.warning(f'Datetime object exlucded. {text}')
            except ValueError:
                logging.warning(f'Not enough values to unpack. {text}')
    logging.info(f'INSERTING records to LEGEND table')
    cursor.executemany('INSERT INTO legend VALUES (?,?,?)', data)
    conn.commit()


def insertMortalityCauseMetaData():
    fpath = os.path.join(MODULE_PATH,'data',MORTALITY_CASE_FILENAME)
    with open(fpath) as f:
        reader = csv.reader(f, delimiter=',')
        next(reader)
        data = [row for row in reader if row[0]]
        logging.info(f'INSERTING records to MATERNITYMORTALITYCAUSE table')
        cursor.executemany('INSERT INTO maternityMortalityCause VALUES (?,?,?)', data)
        conn.commit()
    


def setUp():
    colNames = setupLocalDB()
    # print(colNames)
    insertLegendData()
    insertMortalityCauseMetaData()
    insertCasesData(colNames)
    pass
    

def cleanUp():
    conn.close()


def main():
    setUp()
    cleanUp()


if __name__ == '__main__':
    main()