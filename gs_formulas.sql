--  High-Resk Referrals

--  MATERNAL CONDITIONS
Distinct Count MOTHERID
WHERE DATEDEL=month
AND PREGDEL=1
AND MATDIS=1

--  FETAL CONDITIONS
Distinct Count FETALID
WHERE DATEDEL=month
AND PREGDEL=1
AND FETDIS=1

--  Placenta
, umbilical cord & amniotic fluid
Distinct Count MOTHERID
WHERE DATEDEL=month
AND PREGDEL=1
AND (
    AMF>0
    OR MEM>0
    OR HRG>0
)

--  Dystocia
Distinct Count MOTHERID
WHERE DATEDEL=month
AND PREGDEL=1
AND (
    DYS > 0
)

-- Other labor complications
Distinct Count MOTHERID
WHERE DATEDEL=month
AND PREGDEL=1
AND (
    OR DURDEL>0
    OR PUER>0
    OR PPHRG>0
)


-- MATERNAL OUTCOMES: Deaths
Distinct Count MOTHERID
WHERE DATEDEL=month
AND PREGDEL=1
AND DISMOTH=3



Distinct Count FETALID
WHERE DATEDEL=month
AND PREGDEL=1
DISBB=1